---
layout: fattoide
---
La prima macchinetta a monete nota all'uomo è quella del primo secolo AC di __Erone di Alessandria__, una sorta di Leonardo da Vinci dell'antica Grecia che inventò tra le altre cose anche le prime porte ad apertura automatica, il tasto per strumenti a corda e una rudimentale macchina a vapore chiamata *eolipila*, oltre alla formula per il calcolo dell'area del triangolo che porta il suo nome.

La sua macchina distribuiva acqua santa e fu introdotta per contrastare l'alto numero di scrocconi. Le monete introdotte nella fessura scivolando premevano su una corda tesa e legata a un rubinetto.

In questo fattoide ci concentreremo sulle macchinette da caffé, che già da sole sono comunque un argomento immenso.

Nel mondo il Giappone è il paese che ne ha di più, oltre 5 milioni (una ogni 23 abitanti).
In europa le macchinette sono 3.5 milioni, di cui il 60% serve bevande calde, e metà di queste ultime sono macchine espresso o a chicchi macinati (fonte: [European Vending Association](http://www.vending-europe.eu/en/7-key-figures-on-the-european-vending-market.html?cmp_id=66&news_id=794)) per un totale di 1.1 milioni di macchinette del caffè espresso nel continente, con una macchinetta ogni 500 abitanti. La distribuzione delle macchinette nel territorio dell'EVA è molto squilibrata, infatti l'80% degli apparecchi è in 6 paesi: Italia, Francia, Germania, Paesi bassi, Francia e UK; considerando che la Russia consta di 145 milioni di abitanti, si ha che in Italia il numero di macchinette per abitante è probabilmente molto più alto. [Un sito](http://www.dmcvending.it/dati-sul-vending-in-italia.html) dichiara che c'è una macchinetta ogni 26 abitanti, la cifra è decsamente alta comparata con i dati dell'EVA, che però nella versione completa sono a pagamento e quindi rimarremo nell'ignoranza riguardo a stime più precise.

Sempre secondo il sito di sopra, il 90% delle macchinette in Italia sono installate negli uffici, il 5% in scuole e università.

* sensore di tazza: una fotocellula rileva la presenza di oggetti sotto l'ugello, e permette al consumatore di mettere una sua tazza senza che venga erogato il bicchiere
* bevande gratis in momenti casuali: ogni bevanda consumata ha una probabilità N di rendere gratuita la consumazione successiva
* configurazione master-slave: una macchinetta "centrale" permette di prelevare prodotti in un'altra macchinetta che non ha nessun display o fessura per le monete, risparmiando spazio e manutenzione
* risparmio energetico: le luci e il pre-riscaldamento del boiler si spengono in giorni e orari prestabiliti
* GSM: guasti e mancanza di prodotti vengono segnalati alla manutenzione utilizzando un modem e una carta SIM

Chiavette
========
In grossa parte degli uffici e delle università è possibile acquistare o prendere con una cauzione una chiavetta ricaricabile contenente il credito. Dal momento che il credito caricato su una macchinetta si può usare sulle altre del circuito e l'uso del GSM è raro, è evidente che i dati sul credito sono contenuti nella chiavetta stessa.

Le informazioni precise sulle tecnologie usate non sono facili da reperire, internet è ricca di siti di persone meschine che propinano metodi per avere credito infinito, mentre c'è meno materiale su come funzionino questi sistemi. In generale, le chiavette sono contactless e non hanno batteria; sfruttano infatti un RFID, lo stesso usato per esempio dalle tessere dei messi pubblici: una spira incorporata all'interno permette alla chiametta di alimentare i circuiti temporaneamente senza un contatto fisico con lo stesso principio di funzionamento dei fornelli a induzione, permettendo una comunicazione via radio a brevissima distanza con la circuiteria della chiavetta, che così non ha bisogno né di contatto elettrico diretto né di batterie e può essere totalmente immersa nella plastica.

Il modello più diffuso sembra essere basato sulla componente PCF7931 della Philips. Questo circuito opera a 125kHz (onde radio a bassa frequenza) per l'alimentazione e 2.3 kHz per la comunicazione, e garantisce una persistenza dei dati all'interno per 20 anni. Curiosamente, la scheda tecnica di componente specifica che può funzionare anche mentre sottoposto a accelerazioni di 20g.

La memoria interna è di 1024 bit, più che sufficienti per memorizzare il credito e eventuali seriali. Questa memoria è una EEPROM, basata su un array di transistor in cui il gate di controllo è isolato da un sottilissimo strato di ossido. A seconda del fatto che in questo gate ci sia o meno una carica elettrica, il transistor è aperto o chiuso e si ha lo 0 e 1 logico. Dando al transistor una tensione elettrica maggiore di quella usata per leggerlo si scatena il cosiddetto __effetto tunnel__, un fenomeno studiato dalla fisica quantistica che permette alla carica di "passare" in senso probabilistico attraverso la barriera di ossido e quindi cambiare lo stato logico del singolo transistor.

La comunicazione avviene tramite cambiamenti dell'ampiezza del segnale radio (AKS - Amplitude Key Shifting,la frequenza non viene quindi usata per trasmettere informazioni) e codificando gli 0 e gli 1 con
 Quando la chiavetta viene attivata, trasmette a ciclo continuo il contenuto della sua memoria in sola lettura. La velocità di trasmissione dei dati è di circa 2 kb/s in lettura e 1 in scrittura, quindi la chiavetta viene letta completamente in circa mezzo secondo.

I primi 56 bit di memoria contengono una eventuale password, e se il bit numero 57 è settato a 1 ogni operazione di scrittura deve fornirla per essere accettata. Naturalmente, la password viene letta solo quando non è richiesta (in tal caso i primi 56 bit sono liberamente utilizzabili per dati qualsiasi), altrimenti viene "redatta" e sostituita con una serie di 0.

Software
========

GIGA (uermanualen.pdf): disponibile in varie versioni, basato su .NET e un database SQL Server, psi occupa essenzialmente di tutto ciò che riguarda la gestione di molte macchinette. Oltre a consulytare i log e create e gestire i file di configurazione (con parametri per la preparazione, nomi dei prodotti e configurazioni di prezzi e orari), permette di definire le utenze che possono accedere al sistema e configurare le macchinette e i loro privilegi e addirittura preparare e stampare le etichette.


TODO altri?
